FROM continuumio/anaconda3:4.2.0

RUN apt-get update && \
apt-get install -y \
    git \
    wget \
    zip \
    cmake \
    zlib1g \
    libjpeg-dev \
    libopencv-dev \
    libtbb-dev \
    bzip2 \
    ca-certificates \
    libglib2.0-0 \
    libxext6 \
    libsm6 \
    libxrender1

RUN conda install -y \
    chainer=1.5.0.2 \
    cython=0.23.4 \
    numpy=1.10.1 \
    tqdm

ENV PATH /opt/conda/bin:$PATH
RUN git clone https://gitlab.com/tonychang/ssai-cnn /ssai-cnn
ENV SSAI_HOME /ssai-cnn
RUN wget https://github.com/Itseez/opencv/archive/3.0.0.zip
RUN unzip 3.0.0.zip && rm -rf 3.0.0.zip
RUN mkdir /opencv-3.0.0/build && cp $SSAI_HOME/shells/build_opencv_dev.sh /opencv-3.0.0/build/.
RUN cd /opencv-3.0.0/build && bash ./build_opencv_dev.sh && make -j32 install

RUN cd / && wget http://downloads.sourceforge.net/project/boost/boost/1.59.0/boost_1_59_0.tar.bz2
RUN tar xvf boost_1_59_0.tar.bz2 && rm -rf boost_1_59_0.tar.bz2 
RUN cd boost_1_59_0 && bash ./bootstrap.sh 
RUN cd boost_1_59_0 && ./b2 -j32 install cxxflags="-I/opt/conda/include/python3.5m"

RUN git clone https://github.com/ndarray/Boost.NumPy.git
RUN cd Boost.NumPy && mkdir build && cd build && \
cmake -DPYTHON_LIBRARY=/opt/conda/lib/libpython3.6m.so ../ && make install

RUN bash ./ssai-cnn/scripts/utils/build.sh

